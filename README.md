# README #

This is a repository created as an extension to [Demand forecasting](https://bitbucket.org/aurimassankauskas/demand-forecasting/src/master/). It can be used as a final solution for both demand and supply forecasting, because:

* It includes the data collection scripts from NHSp DWH.
* Covers both supply and demand forecasting as well as gap analysis for the upcoming 18 months.
* Produces the output for the extended Power BI dashboard build.

Please note that it is still not a productionised code and only generates the output for the data set that wass available at the moment of development. The actual implementation still needs to be done in the NHSp environment.

### Main code files: ###

* **00_functions** - includes various self described functions that are used in the other notebooks.
* **01_data_load** - raw data load and preparation (i.e. filtering out irrelevant trusts, combining scarce trust data together, etc.) for the modelling.
* **02_main** - the main directory where all modelling and results extraction takes place. It is enough to run only this part of code, because 00_functions and 01_data_load are being sourced in here.
* **03_param_sel** - this file is used for parameter tuning, but can be considered as a irrelevant script if only the rerun of the model is needed, but no additional parameter tuning is done. It is also a semi-manual (grid selection took too much time) parameter selection and testing based on MAPE outcomes.
